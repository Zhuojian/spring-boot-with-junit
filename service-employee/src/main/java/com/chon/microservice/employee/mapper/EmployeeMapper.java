package com.chon.microservice.employee.mapper;

import com.chon.microservice.employee.dto.EmployeeDto;
import com.chon.microservice.employee.entity.EmployeeEntity;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel="spring")
public interface EmployeeMapper {
    @IterableMapping(qualifiedByName="employeeEntityToEmployeeDto")
    List<EmployeeDto> employeeEntitysToEmployeeDtos(List<EmployeeEntity> employeeEntity);
    @IterableMapping(qualifiedByName="employeeDtoToEmployeeEntity")
    List<EmployeeEntity> employeeDtosToEmployeeEntitys(List<EmployeeDto> employeeDto);


    @Mapping(source = "empName",target = "name")
    @Mapping(source = "empLastname",target = "lastName")
    @Mapping(source = "empId",target = "id")
    EmployeeDto employeeEntityToEmployeeDto(EmployeeEntity employeeEntity);

    @Mapping(source = "name",target = "empName")
    @Mapping(source = "lastName",target = "empLastname")
    @Mapping(source = "id",target = "empId")
    EmployeeEntity employeeDtoToEmployeeEntity(EmployeeDto employeeDto);
}
