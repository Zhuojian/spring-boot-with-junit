package com.chon.microservice.employee.filter;


import com.chon.microservice.employee.client.AuthenticationClient;
import com.chon.microservice.employee.dto.CommonResponseDto;
import com.chon.microservice.employee.dto.TokenDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@RequiredArgsConstructor
public class SecurityFilter implements Filter {

    final AuthenticationClient authenticationClient;

    @Override
    public void doFilter(ServletRequest request, ServletResponse respond, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest)request;
        HttpServletResponse httpRespond = (HttpServletResponse)respond;

        String requestUrl = httpRequest.getRequestURI();
        if(
                requestUrl.startsWith("/swagger-ui.html") ||
                requestUrl.startsWith("/v2/api-docs") ||
                requestUrl.startsWith("/configuration/ui") ||
                requestUrl.startsWith("/swagger-resources") ||
                requestUrl.startsWith("/configuration/security") ||
                requestUrl.startsWith("/webjars")
        ){
            filterChain.doFilter(request, respond);
            return;
        }

        String userToken = httpRequest.getHeader("X-Token");
        if(null != userToken){
            TokenDto tokenDto = new TokenDto();
            tokenDto.setAccessToken(userToken);
            CommonResponseDto commonResponseDto = authenticationClient.verifyToken(tokenDto);
            if(null != commonResponseDto && null !=commonResponseDto.getData()){
                filterChain.doFilter(request, respond);
            }else{
                httpRespond.sendError(HttpServletResponse.SC_UNAUTHORIZED,"");
            }
        }else{
            httpRespond.sendError(HttpServletResponse.SC_UNAUTHORIZED,"");
        }


    }
}
