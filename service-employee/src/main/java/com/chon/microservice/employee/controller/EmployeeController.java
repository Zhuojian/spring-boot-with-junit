package com.chon.microservice.employee.controller;

import com.chon.microservice.employee.dto.CommonResponseDto;
import com.chon.microservice.employee.dto.EmployeeDto;
import com.chon.microservice.employee.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("employee")
@RequiredArgsConstructor
public class EmployeeController {
    final EmployeeService employeeService;

    @PostMapping("")
    public CommonResponseDto insertEmployee(@RequestBody EmployeeDto employeeDto){
        return employeeService.insertEmployee(employeeDto);
    }
    @PutMapping("")
    public CommonResponseDto updateEmployee(@RequestBody EmployeeDto employeeDto){
        return employeeService.updateEmployee(employeeDto);
    }

    @GetMapping("")
    public CommonResponseDto findAllEmployee(){
        return employeeService.findAll();
    }

    @GetMapping("{id}")
    public CommonResponseDto findEmployeeById(@PathVariable String id){
        return employeeService.findById(id);
    }

    @DeleteMapping("")
    public CommonResponseDto deleteEmployeeById(@RequestBody EmployeeDto employeeDto){
        return employeeService.deleteEmployee(employeeDto);
    }

}
