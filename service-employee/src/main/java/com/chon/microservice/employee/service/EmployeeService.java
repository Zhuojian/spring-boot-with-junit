package com.chon.microservice.employee.service;

import com.chon.microservice.employee.dto.CommonResponseDto;
import com.chon.microservice.employee.dto.EmployeeDto;
import com.chon.microservice.employee.entity.EmployeeEntity;
import com.chon.microservice.employee.mapper.EmployeeMapper;
import com.chon.microservice.employee.repository.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class EmployeeService {
    final private EmployeeMapper employeeMapper;

    final private EmployeeRepository employeeRepository;

    public CommonResponseDto insertEmployee(EmployeeDto employeeDto){

        CommonResponseDto commonResponseDto = new CommonResponseDto();
        EmployeeEntity employeeEntity = employeeMapper.employeeDtoToEmployeeEntity(employeeDto);
        if(employeeRepository.insert(employeeEntity) > 0 ){
            commonResponseDto.setStatus("200");
            commonResponseDto.setData(employeeDto);
        }else{
            commonResponseDto.setStatus("500");
        }

        return commonResponseDto;
    }

    public CommonResponseDto updateEmployee(EmployeeDto employeeDto){
        CommonResponseDto commonResponseDto = new CommonResponseDto();
        EmployeeEntity employeeEntity = employeeMapper.employeeDtoToEmployeeEntity(employeeDto);
        if(employeeRepository.update(employeeEntity) > 0 ){
            commonResponseDto.setStatus("200");
            commonResponseDto.setData(employeeDto);
        }else{
            commonResponseDto.setStatus("500");
        }

        return commonResponseDto;
    }

    public CommonResponseDto findAll(){
        CommonResponseDto commonResponseDto = new CommonResponseDto();
        List<EmployeeDto> result = employeeMapper.employeeEntitysToEmployeeDtos(employeeRepository.findAll());
        commonResponseDto.setStatus("200");
        commonResponseDto.setData(result);

        return commonResponseDto;
    }

    public CommonResponseDto findById(String id){
        CommonResponseDto commonResponseDto = new CommonResponseDto();
        try{
            EmployeeDto result = employeeMapper.employeeEntityToEmployeeDto(employeeRepository.findById(id));
            commonResponseDto.setStatus("200");
            commonResponseDto.setData(result);
        }catch (EmptyResultDataAccessException e){
            commonResponseDto.setStatus("404");
        }


        return commonResponseDto;
    }

    public CommonResponseDto deleteEmployee(EmployeeDto employeeDto){
        CommonResponseDto commonResponseDto = new CommonResponseDto();
        EmployeeEntity employeeEntity = employeeMapper.employeeDtoToEmployeeEntity(employeeDto);
        if(employeeRepository.delete(employeeEntity) > 0 ){
            commonResponseDto.setStatus("200");
            commonResponseDto.setData(employeeDto);
        }else{
            commonResponseDto.setStatus("500");
        }

        return commonResponseDto;
    }
}
