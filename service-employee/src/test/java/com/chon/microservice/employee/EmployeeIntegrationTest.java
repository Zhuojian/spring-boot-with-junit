package com.chon.microservice.employee;

import com.chon.microservice.employee.dto.CommonResponseDto;
import com.chon.microservice.employee.dto.EmployeeDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class EmployeeIntegrationTest {
    @Autowired
    private TestRestTemplate restTemplate;

    private String token;

    @Before
    public void setup(){
        this.token = "eyJhbGciOiJIUzI1NiJ9.eyJjdXJyZW50X2RhdGUiOiIyMDIwLTA3LTEzVDIyOjE2OjQ2IiwidXNlcm5hbWUiOiJhZG1pbiIsInN0YXR1cyI6IkEifQ.VJkPz6Rrwiz6TMcDy1FYj4iLtVo4o6XW-j73NgGFyAE";
    }

    @Test
    public void createEmployee(){
        EmployeeDto employeeDto = new EmployeeDto();
        employeeDto.setName("test");
        employeeDto.setLastName("test");
        employeeDto.setId("003");

        restTemplate.getRestTemplate().setInterceptors(
                Collections.singletonList((request, body, execution) -> {
                    request.getHeaders()
                            .add("X-Token",this.token);
                    return execution.execute(request, body);
                }));

        ResponseEntity<CommonResponseDto> responseEntity =
                restTemplate.postForEntity("/employee", employeeDto, CommonResponseDto.class);
        CommonResponseDto commonResponseDto = responseEntity.getBody();

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(commonResponseDto.getStatus()).isEqualTo("200");
    }

    @Test
    public void findOneEmployee(){

        restTemplate.getRestTemplate().setInterceptors(
                Collections.singletonList((request, body, execution) -> {
                    request.getHeaders()
                            .add("X-Token",this.token);
                    return execution.execute(request, body);
                }));

        ResponseEntity<CommonResponseDto> responseEntity =
                restTemplate.getForEntity("/employee/002",CommonResponseDto.class);
        CommonResponseDto commonResponseDto = responseEntity.getBody();

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(commonResponseDto.getStatus()).isEqualTo("200");
    }
}
