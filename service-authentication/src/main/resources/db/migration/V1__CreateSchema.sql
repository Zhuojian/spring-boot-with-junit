DROP TABLE IF EXISTS users;

CREATE TABLE users (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  username VARCHAR(250) NOT NULL,
  password VARCHAR(250) NOT NULL,
  status VARCHAR(1) NOT NULL
);

CREATE TABLE user_token (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  user_id INT   NOT NULL,
  token VARCHAR(250) NOT NULL,
  status VARCHAR(1) NOT NULL
);

INSERT INTO users (username, password, status) VALUES
  ('admin', '81dc9bdb52d04dc20036dbd8313ed055', 'A'),
  ('chon', '81dc9bdb52d04dc20036dbd8313ed055', 'A'),
  ('user1', '81dc9bdb52d04dc20036dbd8313ed055', 'A');