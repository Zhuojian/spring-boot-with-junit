package com.chon.microservice.authen.service;

import com.chon.microservice.authen.dto.CommonResponseDto;
import com.chon.microservice.authen.dto.LoginDto;
import com.chon.microservice.authen.dto.TokenDto;
import com.chon.microservice.authen.dto.UserDto;
import com.chon.microservice.authen.repository.UserRepository;
import com.chon.microservice.authen.util.HashUtil;
import io.jsonwebtoken.SignatureException;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.security.NoSuchAlgorithmException;

@Service
@RequiredArgsConstructor
public class LoginService {
    final private HashUtil hashUtil;
    final private UserRepository userRepository;
    final private TokenProvider tokenProvider;

    public CommonResponseDto login(LoginDto loginDto){
        CommonResponseDto commonResponseDto = new CommonResponseDto();
        try {
            String hashPassword = hashUtil.getHashPassword(loginDto.getPassword());
            UserDto userDto = userRepository.findUserByUsernameAndPassword(loginDto.getUsername(),hashPassword);

            TokenDto tokenDto = new TokenDto();
            tokenDto.setAccessToken(tokenProvider.generateUserToken(userDto));

            commonResponseDto.setStatus("200");
            commonResponseDto.setData(tokenDto);
        }catch (NoSuchAlgorithmException e) {
            commonResponseDto.setStatus("500");
        }catch (EmptyResultDataAccessException e){
            commonResponseDto.setStatus("404");
        }

        return commonResponseDto;
    }

    public CommonResponseDto verify(TokenDto tokenDto){
        CommonResponseDto commonResponseDto = new CommonResponseDto();
        try{
            UserDto userDto = tokenProvider.parseToken(tokenDto.getAccessToken());
            commonResponseDto.setStatus("200");
            commonResponseDto.setData(userDto);
        }catch (SignatureException e){
            commonResponseDto.setStatus("403");
        }
        return commonResponseDto;
    }
}
