package com.chon.microservice.authen.controller;

import com.chon.microservice.authen.dto.CommonResponseDto;
import com.chon.microservice.authen.dto.LoginDto;
import com.chon.microservice.authen.dto.TokenDto;
import com.chon.microservice.authen.service.LoginService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("authentication")
@RequiredArgsConstructor
public class LoginController {
    final private LoginService loginService;

    @PostMapping("login")
    public CommonResponseDto login(@RequestBody LoginDto request){
        return loginService.login(request);
    }

    @PostMapping("verify")
    public CommonResponseDto verify(@RequestBody TokenDto request){
        return loginService.verify(request);
    }
}
