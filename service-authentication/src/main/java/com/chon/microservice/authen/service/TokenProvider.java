package com.chon.microservice.authen.service;

import com.chon.microservice.authen.dto.UserDto;
import com.chon.microservice.authen.property.TokenProp;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

@Getter
@Component
@RequiredArgsConstructor
public class TokenProvider {

    final TokenProp tokenProp;

    public String generateUserToken(UserDto userDto){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        Map<String, Object> claims = new HashMap<>();
        claims.put("username", userDto.getUsername());
        claims.put("status", userDto.getStatus());
        claims.put("current_date", dateFormat.format(Calendar.getInstance().getTime()));

        return Jwts.builder().setClaims(claims)
                .signWith(SignatureAlgorithm.HS256, tokenProp.getSecretKey())
                .compact();
    }

    public UserDto parseToken(String token){

        Claims claims = Jwts.parser()
                .setSigningKey(tokenProp.getSecretKey())
                .parseClaimsJws(token)
                .getBody();

        UserDto userDto = new UserDto();
        userDto.setUsername(claims.get("username").toString());
        userDto.setStatus(claims.get("status").toString());

        return userDto;
    }
}
